﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YankEx
{
    class Program
    {
        static void Main(string[] args)
        {
            int playerCount = 10;
            int giftValueMin = 1;
            int giftValueMax = 10;
            int numberOfSimulations = 1;

            Game Game = new Game(playerCount, giftValueMin, giftValueMax, numberOfSimulations);
            Console.ReadKey();
        }
    }
}
