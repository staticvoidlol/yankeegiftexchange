﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;

namespace YankEx
{
    public static class Helper
    {
        // PSA: C# Random.Next INCLUDES lower bound but EXCLUDES upper bound

        // Randomize a list
        public static void Shuffle<T>(this IList<T> list, Random RNG)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = RNG.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static T DeepClone<T>(T obj)
        {
            T objResult;
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, obj);
                ms.Position = 0;
                objResult = (T)bf.Deserialize(ms);
            }
            return objResult;
        }
        
    }

    public class Game
    {
        List<Player> Players = new List<Player>();
        List<Gift> Gifts = new List<Gift>();
        List<Gift> GiftsUnopened = new List<Gift>();
        List<Gift> GiftsOpened = new List<Gift>();
        int PlayerCount;
        int GiftValueMin;
        int GiftValueMax;
        int NumberOfSimulations;
        private static Random RNG = new Random();

        // Cache prepared data before game for multiple runs
        List<Player> PlayersCached = new List<Player>();
        List<Gift> GiftsCached = new List<Gift>();

        public Game(int players, int giftValueMin, int giftValueMax, int numberOfSimulations)
        {
            // Set params
            PlayerCount = players;
            GiftValueMin = giftValueMin;
            GiftValueMax = giftValueMax;
            NumberOfSimulations = numberOfSimulations;

            // Generate components
            CreatePlayers();
            CreateGifts();

            PrepareGame();
            Start();
        }

        protected void CreatePlayers()
        {
            string namePrefix = @"Player ";

            for (int i = 0; i < PlayerCount; i++)
            {
                Players.Add(new Player(namePrefix + (i + 1).ToString().PadLeft(PlayerCount.ToString().Length, '0'), i + 1));
            }
        }

        protected void CreateGifts()
        {
            string namePrefix = @"Gift ";
            Random r = new Random();

            for (int i = 0; i < PlayerCount; i++)
            {
                Gift g = new Gift(namePrefix + (i + 1).ToString().PadLeft(PlayerCount.ToString().Length, '0'), i + 1, r.Next(GiftValueMin, GiftValueMax + 1)); // Upper bound excluded

                Gifts.Add(g);
                //GiftsUnopened.Add(g);
            }
        }

        protected void RandomizePlayerOrder()
        {
            Helper.Shuffle(Players, RNG);
        }

        protected void ListPlayers()
        {
            for (int i = 0; i < Players.Count; i++)
            {
                Console.WriteLine(Players[i].Name);
            }
        }

        protected void ListGifts()
        {
            for (int i = 0; i < Gifts.Count; i++)
            {
                Console.WriteLine(Gifts[i].Name + @" @ " + Gifts[i].Value.ToString());
            }
        }

        public int GetNumberOfOpenedGifts()
        {
            int count = 0;
            for (int i = 0; i < Gifts.Count; i++)
            {
                if (!Gifts[i].IsOpen)
                    count++;
            }

            return count;
        }

        protected void GiftsUpdateUnopened()
        {
            GiftsOpened.Clear();
            GiftsUnopened.Clear();

            for (int i = 0; i < Gifts.Count; i++)
            {
                Gift giftCurr = Gifts[i];

                if (giftCurr.IsOpen)
                    GiftsOpened.Add(giftCurr);
                else
                    GiftsUnopened.Add(giftCurr);
            }
        }

        protected void PrepareGame()
        {
            // List gifts
            Console.WriteLine("Gifts: ");
            ListGifts();

            // List players before randomisation
            Console.WriteLine("Players before random: ");
            ListPlayers();

            // Determine Random order of players
            RandomizePlayerOrder();
            Console.WriteLine("Players after random: ");
            ListPlayers();

            // Save cached values
            PlayersCached = Helper.DeepClone(Players);
            GiftsCached = Helper.DeepClone(Gifts);
        }

        protected void Start()
        { 
            Console.WriteLine("*********************");
            Console.WriteLine("*** Starting Game ***");
            Console.WriteLine("*********************");

            Strategy strat;

            // Loop through all strategies
            for (int x = 0; x < 1; x++)
            {
                if (x == 0)
                    strat = Strategy.Cornel;
                else if (x == 1)
                    strat = Strategy.Pierre;
                else
                    strat = Strategy.Random;

                using (StreamWriter w = new StreamWriter(@"C:\results_" +strat.ToString() +@".csv"))
                {
                    // Write  header
                    for (int i = 0; i < PlayerCount; i++)
                    {
                        if (i == PlayerCount - 1)
                            w.WriteLine(@"PlayerPos " + (i + 1).ToString());
                        else
                            w.Write(@"PlayerPos " + (i + 1).ToString() + @";");
                    }

                    for (int j = 0; j < NumberOfSimulations; j++)
                    {
                        // Re-init arrays so that all runs and  strategies use the same gifts and players as a starting point
                        Players = Helper.DeepClone(PlayersCached);
                        Gifts = Helper.DeepClone(GiftsCached);

                        for (int i = 0; i < PlayerCount; i++)
                        {
                            Console.WriteLine(@"----------------------------");
                            // Update opened and unopened gifts
                            GiftsUpdateUnopened();

                            // Expedient current player ref
                            Player playerCurrent = Players[i];

                            // Ask player whose gift they'd like to steal - if 0, they want to open new gift instead
                            int stealFrom = playerCurrent.GetChoiceSteal(RNG, Gifts, GiftsUnopened, GiftsOpened, strat);

                            if (stealFrom == 0)
                            {
                                // Player is not stealing someone else's gift, so let's ask them to choose one at random
                                Gift newGift = playerCurrent.GetChoiceRandomGift(RNG, GiftsUnopened);
                                playerCurrent.ReceiveGiftToOpen(newGift);
                            }
                            else
                            {
                                Console.WriteLine("player is choosing to steal.");
                                // Find player with index = stealfrom
                                Player playerToStealFrom = Players.Find(item => item.Index == stealFrom);

                                // Transfer ownership
                                playerCurrent.ReceiveGiftAlreadyOpened(playerToStealFrom.GiftCurrent);

                                // Give player who was stolen from a new unopened gift
                                playerToStealFrom.ReceiveGiftToOpen(playerCurrent.GetChoiceRandomGift(RNG, GiftsUnopened));
                            }

                            // Update opened and unopened gifts
                            GiftsUpdateUnopened();
                            Console.WriteLine(@"----------------------------");
                        }

                        // Ensure we've processed all
                        if (GiftsOpened.Count == Gifts.Count)
                            Console.WriteLine("OK. All gifts opened.");
                        else
                            Console.WriteLine("ERROR! Not all gifts opened!");

                        if (GiftsUnopened.Count == 0)
                            Console.WriteLine("OK. No gifts unopened.");
                        else
                            Console.WriteLine("ERROR! Some unopened gifts left!");

                        // Check that all players have a gift
                        Console.WriteLine(@"=========================");
                        for (int i = 0; i < PlayerCount; i++)
                        {
                            Console.WriteLine(Players[i].Name + " has " + Players[i].GiftCurrent.Name + " valued at " + Players[i].GiftCurrent.Value.ToString());
                        }
                        Console.WriteLine(@"=========================");

                        // Do analysis on how good the outcome was
                        //WriteResultsToFile();
                        for (int i = 0; i < PlayerCount; i++)
                        {
                            if (i == PlayerCount - 1)
                                w.WriteLine(Players[i].GiftCurrent.Value.ToString());
                            else
                                w.Write(Players[i].GiftCurrent.Value.ToString() + @";");
                        }

                        Console.WriteLine("*********************");
                    }
                }
            }
        }

        public static DirectoryInfo GetExecutingDirectory()
        {
            var location = new Uri(Assembly.GetEntryAssembly().GetName().CodeBase);
            return new FileInfo(location.AbsolutePath).Directory;
        }

        protected void WriteResultsToFile()
        {
            //DirectoryInfo currDir = GetExecutingDirectory();

            //FileInfo f = new FileInfo(Path.Combine(currDir.FullName, @"results.csv"));

            FileInfo f = new FileInfo(@"c:\results.csv");

            using(StreamWriter w =  new StreamWriter(f.FullName))
            {
                for (int i = 0; i < PlayerCount; i++)
			    {
                    if(i == PlayerCount - 1)
                        w.WriteLine(Players[i].Name);
                    else
                        w.Write(Players[i].Name +@";");
			    }

                for (int i = 0; i < PlayerCount; i++)
                {
                    if (i == PlayerCount - 1)
                        w.WriteLine(Players[i].GiftCurrent.Value.ToString());
                    else
                        w.Write(Players[i].GiftCurrent.Value.ToString() + @";");
                }
                
            }
        }
    }
}
