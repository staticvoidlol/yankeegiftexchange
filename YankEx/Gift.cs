﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YankEx
{
    [Serializable]
    public class Gift
    {
        public string Name;
        public int Index;
        public int Value;
        public List<Player> Owners = new List<Player>();
        public bool IsOpen = false;

        public Gift(string name, int index, int value)
        {
            Name = name;
            Index = index;
            Value = value;
        }

        public void Say(string message)
        {
            Console.WriteLine(Name + " says: " + message);
        }

        public void Open(Player playerOpened)
        {
            //Say("I've been opened by " + playerOpened.Name);
            IsOpen = true;
            Owners.Add(playerOpened);
        }

        public void TransferTo(Player playerToTransferTo)
        {
            Owners.Add(playerToTransferTo);
        }

        public Player GetOwnerCurrent()
        {
            return Owners.Last();
        }
    }
}
