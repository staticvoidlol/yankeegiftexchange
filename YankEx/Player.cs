﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YankEx
{
    public enum Strategy
    {
        Random,
        Cornel,
        Pierre
    }
    
    [Serializable]
    public class Player
    {
        public string Name;
        public int Index;
        public Gift GiftCurrent;
        public List<Gift> GiftsHistory;

        public Player(string name, int index)
        {
            Name = name;
            Index = index;
        }

        public void Say(string message)
        {
            Console.WriteLine(Name + @" says: " + message);
        }

        // Returns which person's gift they want to steal
        public int GetChoiceSteal(Random r, List<Gift> giftsAll, List<Gift> giftsUnopened, List<Gift> giftsOpened, Strategy strategy)
        {
            if (giftsUnopened.Count == giftsAll.Count)
            {
                Say("I am first, so I'm opening a new gift.");
                return 0;
            }
            else
            {
                switch (strategy)
                {
                    case Strategy.Random:
                        return 0;
                    case Strategy.Cornel:
                        {
                            // Get avg value of all open gifts
                            float giftOpenedValueAvg = (float)(giftsOpened.Sum(item => item.Value)) / (float)giftsOpened.Count;
                            Say("I've determined the avg opened gift value is: " + giftOpenedValueAvg.ToString());

                            // Get max value of all opened gifts
                            int giftOpenedValueMax = giftsOpened.Max(item => item.Value);

                            // If any opened gift is worth more than the avg, we steal that (max) one
                            for (int i = 0; i < giftsOpened.Count; i++)
                            {
                                Gift giftConsidered = giftsOpened[i];

                                if (giftConsidered.Value > giftOpenedValueAvg && giftConsidered.Value == giftOpenedValueMax)
                                {
                                    Say("I'm gonna steal this person's gift: Player Name=" + giftConsidered.Owners.Last().Name + "|PlayerIndex=" + giftConsidered.Owners.Last().Index.ToString() +"|GiftValue="+giftConsidered.Value.ToString());
                                    return giftConsidered.Owners.Last().Index;
                                }
                            }
                                                        
                            Say("No opened gifts valuable enough found to steal. AVG Value=" + giftOpenedValueAvg.ToString() + "|Max Openend Gift Value=" + giftOpenedValueMax.ToString());
                            return 0;
                        }

                    case Strategy.Pierre:
                        return 0;
                    default:
                        break;
                }

                return 0;
            }
        }

        public Gift GetChoiceRandomGift(Random r, List<Gift> giftsUnopened)
        {
            return giftsUnopened[r.Next(0, giftsUnopened.Count - 1)];
        }

        public void ReceiveGiftToOpen(Gift gift)
        {
            Say("Opening a gift: " + "Gift Name=" + gift.Name + "|Value=" + gift.Value.ToString());
            gift.Open(this);
            GiftCurrent = gift;
        }

        public void ReceiveGiftAlreadyOpened(Gift gift)
        {
            
            Say("I am given open gift: " + "Gift Name=" + gift.Name + "|Value=" + gift.Value.ToString());
            gift.TransferTo(this);
            GiftCurrent = gift;
        }
    }
}
